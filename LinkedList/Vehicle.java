class Vehicle
{
	int id;
	int move;
	
	public Vehicle(int n){
		id = n;
		move = 0;
	}
	
	public int incMove(int n){
		return (move + n);
	}
}
