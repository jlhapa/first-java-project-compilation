import java.util.LinkedList;
import java.util.*; 
import java.util.LinkedList;

class Main
{
    public static Boolean displayParkingStatus(LinkedList<Vehicle> list, int slots, Boolean flag) {
        int numOfCars = list.size();
		int vehicleID = 0;
		System.out.print("Parkign IDs : ");		
        for(int x = 0; x < numOfCars; x++){
            vehicleID=list.get(x).id;
			System.out.print("[" + vehicleID + "]" + (x != numOfCars-1 ? ", " : "\n"));			
		}
		if(flag) {
			return (slots-numOfCars) > 0 ? true : false;
		}
		System.out.println((slots-numOfCars) > 0 ? "We have [" + (slots-numOfCars) + "] slots available for you." : "Sorry, but we're fully booked.");   
		return false; 
	}
	
	public static void displayList(LinkedList<Vehicle> list) {
		Vehicle vehicle = null;
		for(int x = 0; x < list.size(); x++){
			vehicle=list.get(x);
			System.out.print("[" + vehicle.id + "]" + (x != list.size()-1 ? ", " : "\n"));
		}
	}

	public static void displayLists(LinkedList<Vehicle> parkingList, LinkedList<Vehicle> waitingList, int userInput) {
		switch(userInput) { 
			case 1:
				if(parkingList.size() == 0) {
					System.out.println("Nothing to show.");
					break;
				}						
				displayList(parkingList);
				break;
			case 2:
				if(waitingList.size() == 0) {
					System.out.println("Nothing to show.");
					break;
				}
				displayList(waitingList);
				break;
		}
	}

public static void main(String args[]) {	
	LinkedList<Vehicle> parkingList = new LinkedList<Vehicle>();
    LinkedList<Vehicle> waitingList = new LinkedList<Vehicle>();
    int slots = 3;    
	int choice = 0;
    Scanner userInput = new Scanner(System.in);
    String lines="-----------------------------------------------------------------------";    
    System.out.println(lines + "\n" + String.format("%" + ((lines.length() / 2) - 6)+ "s", "") + "Car Parking Simulator" +
    "\n" + lines);
    System.out.println("Number of parking slots: " + slots + "\n");
    
	try {
        while(true) { 
            System.out.println("[1] Enter Parking Space\n[2] Exit \n[3] View Lists\n[4] Quit"); 
            System.out.print("Enter choice ");
	    	choice = userInput.nextInt(); 
                switch(choice) { 
                    case 1:
                        if(parkingList.size() < slots) {		
                            displayParkingStatus(parkingList, slots, false);
                            System.out.print("Confirm y? Y/N ");
                            String confirm=userInput.next();
                            if(confirm.equalsIgnoreCase("y")) {
                                System.out.println("\nYour parking ID is: [" + (parkingList.size()+1) + "], use it when exiting the parking space.");
                                parkingList.add(new Vehicle(parkingList.size()+1));
                            }
                        }
                        else {
                            System.out.println("There's no slot available for now.");
                            Boolean userInput2;
                            System.out.println(waitingList.size() + " customers are currently on our parking queue.");
                            System.out.print("Willing to wait? Y/N ");
                            String str = userInput.next();
                            userInput2 = str.equalsIgnoreCase("y") ? true : false;
                            if(userInput2) {
                                int waitingCar = (waitingList.size() + 1) + slots;
                                    for(int i = 0; i < waitingList.size() - 1; i++) {
                                        Vehicle temp = waitingList.get(i);
                                        System.out.println(temp.id);
                                    }
                                    waitingList.add(new Vehicle(waitingCar));
                                    System.out.println("We have [" + waitingList.size() + "] " + (waitingList.size() > 1 ? "customers" : "customer") + " on the Waiting List ");
                            }
                            else {
							    System.out.println("Good Bye!");
                            }
				}
                break;				
                    case 2:
					System.out.println("1. From Parking Lot\n2. From Waiting List");
					int userInput3 = userInput.nextInt();
					try{
						switch(userInput3) {
							case 1:
								if(parkingList.size() == 0) {
									System.out.println("Parking space is empty right now.");
								}
								else {
									System.out.println("Cars currently in the parking spaces.");
									Vehicle parkedCar = null;
									System.out.print("\nParking IDs : ");
									for(int h = 0; h < parkingList.size(); h++) {
										parkedCar = parkingList.get(h);										
										System.out.print("[" + parkedCar.id + "]" + (h != parkingList.size()-1 ? ", " : "\n"));
									}											
									System.out.println("Please enter your parking ID to proceed exit.");
									int toBeRemoved = userInput.nextInt();
									for(int h = 0; h < parkingList.size(); h++) {
										if(parkingList.get(h).id != toBeRemoved) {
											continue;
										}
										else {
											Vehicle fromPark = null;
											for(int u = 0; u < parkingList.size() - 1; u++) {
												fromPark = parkingList.get(u);
												if(fromPark.id != toBeRemoved){
													fromPark.move += 2;
												}
												else
													break;
											}
											for(int v = parkingList.size(); (parkingList.get(h).id != toBeRemoved); v--) {
												fromPark.move+=1;
											}
											System.out.println("Number of moves "+ parkingList.get(h).incMove(1));
											System.out.println("Car with paring id "+ parkingList.get(h).id + ", exited the park.");
											parkingList.remove(h);
											break;
										}
									}
									if(waitingList.size() > 0) {
										System.out.println(waitingList.getFirst().id + " parking id is next to enter the park");
										parkingList.add(new Vehicle(waitingList.getFirst().id));
										waitingList.remove(0);
										displayParkingStatus(parkingList, slots, true);
									}
									else {
										System.out.println("There's no car on the waiting list.");
									}
								}
								break;
							case 2: 
								if(waitingList.size() == 0){
									System.out.println("There's no car waiting in the list");
								}
								else {
									System.out.println("There are " + waitingList.size() + " Cars in waiting list");
									System.out.print("Enter your car parking ID: ");
									int toBeRemovedId=userInput.nextInt();
									Vehicle toBeRemoved = null;
									for(int h = 0; h < waitingList.size(); h++) {
										if(waitingList.get(h).id == toBeRemovedId) {
											waitingList.remove(h);
											System.out.println("Car of parking id, " + toBeRemoved + "has left.");
										}
									}
								}
								break;
						}
					}
					catch(Exception e) {
						System.out.println("Invalid input, try again.");
					}
					break;			
				case 3:
					System.out.println("1.Parking List \n2.Waiting list");
					int userInput4 = userInput.nextInt();
					try {
						if(waitingList.size() == 0 && parkingList.size() == 0) {
							System.out.println("Nothing to show.");
							break;
						}
						System.out.print("Parking IDs : ");
						displayLists(parkingList, waitingList, userInput4);
					}
					catch(Exception e) {
						System.out.println("Invalid input, try again");
					}	
					break;
				case 4:
                        System.out.println("Good Bye!");
				
				
				default: System.exit(0);		
			}
		} 
	}
	catch(Exception e) {
		System.out.println("Invalid input, try again.");
	} 

  }          
}  
