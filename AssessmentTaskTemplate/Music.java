public class Music {
	private String title;
	private String singer;
	private String category;

	public Music(String title, String singer, String category) {
		this.title = title;
		this.singer = singer;
		this.category = category;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

	public void setSinger(String singer) {
		this.singer = singer;
	}

	public String getSinger() {
		return singer;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCategory() {
		return category;
	}
}
