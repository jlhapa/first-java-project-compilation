public class Node {
	private String key;
	private Node left;
	private Node right;

	public Node(String key) {
		this.key = key;
		left = null;
		right = null;
	}

	public String getKey() {
		return key;
	}

	public Node getLeft() {
		return left;
	}

	public void setLeft(Node left) {
		this.left = left;
	}

	public Node getRight() {
		return right;
	}

	public void setRight(Node right) {
		this.right = right;
	}
}
