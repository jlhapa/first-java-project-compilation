import java.util.Scanner;

public class Main {
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
		MusicDirectory musicDirectory = new MusicDirectory();

		while(true) {
			printChoices();
			int choice = sc.nextInt();
			switch(choice) {
				case 0:
					return;
				case 1:
					//fix scanner issue of spacing in the argument
					System.out.print("Enter title: ");
					String title = sc.next().toUpperCase();
					System.out.print("Enter singer: ");
					String singer = sc.next().toUpperCase();
					System.out.print("Enter category: ");
					String category = sc.next().toUpperCase();
					System.out.println(musicDirectory.addMusic(new Music(title, singer, category)));
					break;
				case 2:
					//sort by title
					System.out.println(musicDirectory.getAllMusicSortedByTitle());
					break;
				case 3:
					//sort by singer
					System.out.println(musicDirectory.getAllMusicSortedBySinger());
					break;
				case 4:
					//sort by category
					System.out.println(musicDirectory.getAllMusicSortedByCategory());
					break;
				case 5:
					//in-order search
					musicDirectory.inorder();
					break;
				case 6:
					//pre-order search
					musicDirectory.preorder();
					break;
				case 7:
					//post-order search
					musicDirectory.postorder();
					break;
				default:
					System.out.println("\n\t##########################");
					System.out.println("\tNot part of the choices!\n\tPlease choose again!");
					System.out.println("\t##########################\n");
					break;
			}
		}
	}

	public static void printChoices() {
		System.out.println("What do you want to do?");
		System.out.println("\n(0)Exit\n(1)Save new song\n(2)Sort by title\n(3)Sort by singer\n(4)Sort by category");
		System.out.println("(5)In-order Search\n(6)Pre-order Search\n(7)Post-order Search\n");
	}
}
