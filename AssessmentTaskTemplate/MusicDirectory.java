import java.util.*;
import java.text.Collator;

public class MusicDirectory {
	private LinkedList<Music> musicLists;
	private BinarySearchTree tree;

	public MusicDirectory() {
		musicLists = new LinkedList<Music>();
		tree = new BinarySearchTree();
	}

	public String addMusic(Music song) {
		StringBuffer br = new StringBuffer("");

		if (validateMusic(song)) {
			musicLists.add(song);
			insertToTree(song.getTitle());
			br.append("\n\t************************\n");
			br.append("\tSuccess saving new song!\n");
			br.append("\t************************\n");
		}
		else {
			br.append("\n\t****************************************\n");
			br.append("\tAlready have that song in the directory!\n");
			br.append("\t****************************************\n");
		}

		return(br.toString());

	}

	private boolean validateMusic(Music song) {
		boolean checker = true;

		for (Music list : musicLists) {
			if ((list.getTitle().equals(song.getTitle())) &&
				(list.getSinger().equals(song.getSinger())) &&
				(list.getCategory().equals(song.getCategory()))) {
				checker = false;
			}
		}

		return(checker);
	}

	public String getAllMusicSortedByTitle() {
		Collator collator = Collator.getInstance(Locale.US);

		Collections.sort(musicLists, new Comparator<Music>() {
			@Override
			public int compare(Music m1, Music m2) {
				return collator.compare(m1.getTitle(), m2.getTitle());
			}
		});

		return(getSortedMusicLists(musicLists));
	}

	public String getAllMusicSortedBySinger() {
		Collator collator = Collator.getInstance(Locale.US);

		Collections.sort(musicLists, new Comparator<Music>() {
			@Override
			public int compare(Music m1, Music m2) {
				return collator.compare(m1.getSinger(), m2.getSinger());
			}
		});

		return(getSortedMusicLists(musicLists));
	}

	public String getAllMusicSortedByCategory() {
		Collator collator = Collator.getInstance(Locale.US);

		Collections.sort(musicLists, new Comparator<Music>() {
			@Override
			public int compare(Music m1, Music m2) {
				return collator.compare(m1.getCategory(), m2.getCategory());
			}
		});

		return(getSortedMusicLists(musicLists));
	}

	private String getSortedMusicLists(LinkedList<Music> lists) {
		StringBuffer br = new StringBuffer("");

		br.append("=============================================\n");
		for (Music list : lists) {
			br.append("\t***************************\n");
			br.append("\tTitle: " + list.getTitle() + "\n");
			br.append("\tSinger: " + list.getSinger() + "\n");
			br.append("\tCategory: " + list.getCategory() + "\n");
			br.append("\t***************************\n");
		}
		br.append("=============================================\n");

		return(br.toString());
	}

	private void insertToTree(String song) {
		tree.insert(song);
	}

	public void inorder() {
		tree.inorder();
	}

	public void preorder() {
		tree.preorder();
	}

	public void postorder() {
		tree.postorder();
	}

	//sort by title,singer,category using binary search tree
	//refactor sorting in linked list
}
