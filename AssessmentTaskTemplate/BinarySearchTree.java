import java.util.*;
import java.text.Collator;

public class BinarySearchTree {
	Node root;

	public BinarySearchTree() {
		root = null;
	}

	public void insert(String key) {
		root = insertRec(root, key);
	}

	//refactor insertRec method
	private Node insertRec(Node root, String key) {
		ArrayList<String> musicLists = new ArrayList<String>();

		if (root == null) {
			root = new Node(key);
			return root;
		}

		musicLists.add(key);
		musicLists.add(root.getKey());

		Collator collator = Collator.getInstance(Locale.US);

		Collections.sort(musicLists, new Comparator<String>() {
			@Override
			public int compare(String m1, String m2) {
				return collator.compare(m1, m2);
			}
		});

		if(musicLists.get(0).equals(key))
			root.setLeft(insertRec(root.getLeft(), key));
		else if(musicLists.get(1).equals(key))
			root.setRight(insertRec(root.getRight(), key));

		return root;
	}

	//in order search
	public void inorder() {
		inorderRec(root);
	}

	private void inorderRec(Node root) {
		if (root != null) {
			inorderRec(root.getLeft());
			System.out.println(root.getKey());
			inorderRec(root.getRight());
		}
	}

	//pre order search
	public void preorder() {
		preorderRec(root);
	}

	private void preorderRec(Node root) {
		if (root != null) {
			System.out.println(root.getKey());
			preorderRec(root.getLeft());
			preorderRec(root.getRight());
		}
	}

	//post order search
	public void postorder() {
		postorderRec(root);
	}

	private void postorderRec(Node root) { 
		if (root != null) {
			postorderRec(root.getLeft());
			postorderRec(root.getRight());
			System.out.println(root.getKey());
		}
	}
}
