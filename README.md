# First Java Project Compilation

## *Binary Search Tree*

Create a program that will store data in a tree structure. Let the user search the content using `Binary Search Tree`. Implement the concept of preorder and postorder.

## *LinkedList*

Create a parking space simulation. Use `LinkedList` data structure. Let the user park in open space, track used or open space and freed the used space once the user exit the parking space.

## *Assessment Task*

Create a program that store music information or music directory. Let the user sort the content by name or the title. Implement any data structure and `tree sorting algorithms`. 
