import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		BinarySearchTree tree = new BinarySearchTree();
		tree.insert(50);
		tree.insert(30);
		tree.insert(20);
		tree.insert(40);
		tree.insert(70);
		tree.insert(60);
		tree.insert(80);

		System.out.println("Choose a Binary Search Type:\n(1)In-order\n(2)Pre-order\n(3)Post-order");
		int choice = sc.nextInt();
		switch(choice) {
			case 1:
				tree.inorder();
				break;
			case 2:
				tree.preorder();
				break;
			case 3:
				tree.postorder();
				break;
			default:
				System.out.println("Your choice was not part of the Options!");
		}
	}
}
