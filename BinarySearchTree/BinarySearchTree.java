public class BinarySearchTree {
	Node root;

	public BinarySearchTree() {
		root = null;
	}

	public void insert(int key) {
		root = insertRec(root, key);
	}

	private Node insertRec(Node root, int key) {
		if (root == null) {
			root = new Node(key);
			return root;
		}

		if(key < root.getKey())
			root.setLeft(insertRec(root.getLeft(), key));
		else if(key > root.getKey())
			root.setRight(insertRec(root.getRight(), key));

		return root;
	}

	//in order search
	public void inorder() {
		inorderRec(root);
	}

	private void inorderRec(Node root) {
		if (root != null) {
			inorderRec(root.getLeft());
			System.out.println(root.getKey());
			inorderRec(root.getRight());
		}
	}

	//pre order search
	public void preorder() {
		preorderRec(root);
	}

	private void preorderRec(Node root) {
		if (root != null) {
			System.out.println(root.getKey())
			preorderRec(root.getLeft());
			preorderRec(root.getRight());
		}
	}

	//post order search
	public void postorder() {
		postorderRec(root);
	}

	private void postorderRec(Node root) { 
		if (root != null) {
			postorderRec(root.getLeft());
			postorderRec(root.getRight());
			System.out.println(root.getKey());
		}
	}
}
